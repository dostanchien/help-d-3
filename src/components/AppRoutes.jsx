import React from 'react';
import PropTypes from 'prop-types';
import {Routes, Route} from "react-router-dom";
import FavoritePage from '../pages/FavoritePage/FavoritePage';
import CartPage from '../pages/CartPage/CartPage';
import NotPage from '../pages/NotPage/NotPage';
import HomePage from '../pages/HomePage/HomePage';

const AppRoutes = ({favorite, productArray,carts, handleModal,handleCurrentPost,onClickIcon})  =>{
   
<Routes>
      <Route path="/" element={<HomePage favorite={favorite} date={productArray} handleModal={handleModal} handleCurrentPost={handleCurrentPost}
				onClickIcon={onClickIcon}/>}/>;
      <Route path="favorite" element={<FavoritePage favorite={favorite} date={favorite} handleModal={handleModal} handleCurrentPost={handleCurrentPost}
				onClickIcon={onClickIcon}/>}/>;
	  <Route path="cart" element={<CartPage favorite={favorite} date={carts} handleModal={handleModal} handleCurrentPost={handleCurrentPost}
				onClickIcon={onClickIcon}/>}/>;
		<Route path="*" element={<NotPage/>}/>;
</Routes>
}


AppRoutes.propTypes = {
	productArray: PropTypes.array,
	  favorite: PropTypes.array,
	  date: PropTypes.array,
	handleModal: PropTypes.func,
	handleCurrentPost: PropTypes.func,
	onClickIcon: PropTypes.func,
  }
export default AppRoutes

